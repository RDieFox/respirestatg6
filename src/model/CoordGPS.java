package model;

public class CoordGPS {
	private double latitude;
	private double longitude;
	private static double latitudeP0 = 2.348064345917045; //Latitude du Point Zéro des Routes de France
	private static double longitudeP0 = 48.85365230001516; //Longitude du Point Zéro des Routes de France
	
	public CoordGPS(String geometry) {
		this(getLatitudeFromGeometry(geometry),getLongitudeFromGeometry(geometry));
	}
	public CoordGPS(double lat, double lon) {
		latitude = lat;
		longitude = lon;
	}
	
	private static String[] extractLatLongFromGeometry(String geometry) {
		String extract = geometry.substring(2, geometry.length()-1);
		String[] coord = extract.split(", ");
		return coord;
	}
	private static double getLatitudeFromGeometry(String geometry) {
		return Double.parseDouble(extractLatLongFromGeometry(geometry)[0]);
	}
	private static double getLongitudeFromGeometry(String geometry) {
		return Double.parseDouble(extractLatLongFromGeometry(geometry)[1]);
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	public double distance() {
		double latRad = Math.toRadians(latitude);
		double lonRad = Math.toRadians(longitude);
		double latP0Rad = Math.toRadians(latitudeP0);
		double lonP0Rad = Math.toRadians(longitudeP0);
		
		double deltalat = latP0Rad - latRad;
		double deltalon = lonP0Rad - lonRad;
		double a = Math.pow(Math.sin(deltalat / 2), 2) 
				+ Math.cos(latP0Rad) * Math.cos(latRad) 
				* Math.pow(Math.sin(deltalon / 2), 2);
		
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d = 6371 * c;
		return Math.round(d*100.0)/100.0;
	}
	
	
	
	@Override
	public String toString() {
		return "CoordGPS [latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	

	
	

	
}
